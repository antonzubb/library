<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register',   ['as' => 'register', 'uses' => 'RegistrationController@register']);

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix'=>'authors','as'=>'authors.'], function(){
    Route::get('/',          ['as' => 'index', 'uses'  => 'AuthorController@index']);
    Route::post('/create',   ['as' => 'create', 'uses' => 'AuthorController@create']);
    Route::get('/{author}',  ['as' => 'show', 'uses'   => 'AuthorController@show']);
});

Route::group(['prefix'=>'categories','as'=>'authors.'], function(){
    Route::get('/',                  ['as' => 'index', 'uses'  => 'CategoryController@index']);
    Route::post('/create',           ['as' => 'create', 'uses' => 'CategoryController@create']);
    Route::put('/update/{category}', ['as' => 'update', 'uses' => 'CategoryController@update']);
    Route::get('/{category}',        ['as' => 'show', 'uses' => 'CategoryController@show']);
});

Route::group(['prefix'=>'books','as'=>'books.'], function(){
    Route::get('/',              ['as' => 'index', 'uses'  => 'BookController@index']);
    Route::post('/create',       ['as' => 'create', 'uses' => 'BookController@create']);
    Route::get('/{book}',        ['as' => 'show',   'uses'   => 'BookController@show']);
});
