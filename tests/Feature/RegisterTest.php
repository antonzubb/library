<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\{RefreshDatabase,WithFaker};
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * Testing registration of a user
     * @test
     * @return void
     */

    use WithFaker, RefreshDatabase;

    public function a_user_can_register()
    {
        $payload = [
            'name'     => 'user',
            'email'    => 'user@user.com',
            'password' => 'password',
        ];

        $response = $this->json('POST', 'api/register', $payload);

        $response->assertStatus(200)
            ->assertJsonStructure(['access_token', 'token_type', 'expires_in']);

        $this->assertDatabaseHas('users', ['email' => $payload['email']]);
    }


}
