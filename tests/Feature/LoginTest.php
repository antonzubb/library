<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\{RefreshDatabase,WithFaker};
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * Testing login with JWT token
     * @test
     * @return void
     */

    use WithFaker, RefreshDatabase;

    public function a_user_can_login()
    {
        $user = factory(User::class)->create();
        $payload = [
          'email'    => $user->email,
          'password' => 'password',
        ];

        $response = $this->postJson('/api/auth/login', $payload);

        $response->assertStatus(200)
            ->assertJsonStructure(['access_token', 'token_type', 'expires_in']);

    }
}
