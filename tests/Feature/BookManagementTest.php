<?php

namespace Tests\Feature;

use App\Models\Book;
use Illuminate\Foundation\Testing\{RefreshDatabase,WithFaker};
use Tests\TestCase;

class BookManagementTest extends TestCase
{
    /**
     * Testing accessing a whole collections of a books
     * @test
     * @return void
     */

    use WithFaker, RefreshDatabase;

    public function a_user_can_see_all_books()
    {
        $collection = factory(Book::class,10)->create();

        $book = $collection->first();

        $payload = [
            'title'       => $book->title,
            'description' => $book->description,
        ];

        $response = $this->getJson('/api/books');

        $response->assertJsonFragment($payload)
            ->assertOk();
    }

    /**
     * Testing creating book
     * @test
     * @return void
     */
    public function user_can_add_a_book()
    {
        $this->singIn();
        $book = factory(Book::class)->raw();
        $response = $this->postJson(route('books.create'), $book);

        $response->assertStatus(201);
    }

    /**
     * Test unauthenticated user can`t create a book
     * @test
     * @return void
     */
    public function only_authenticated_user_can_add_a_book()
    {
        $book = factory(Book::class)->raw();
        $response = $this->postJson(route('books.create'), $book);

        $response->assertStatus(403);
    }

}
