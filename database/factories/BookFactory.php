<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoryResource;
use Faker\Generator as Faker;

$factory->define(\App\Models\Book::class, function (Faker $faker) {
    return [
        'title'       => $faker->word,
        'description' => $faker->text('1000'),
        'author_id'   => factory(\App\Models\Author::class),
        'category_id' => factory(\App\Models\Category::class),
        'year'        => $faker->year,
    ];
});
