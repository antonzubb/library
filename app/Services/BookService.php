<?php

namespace App\Services;

use App\Models\Book;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class BookService
{
    /**
     * @param array $data
     * @return Book
     */
    public function create(array $data): Book
    {
       return Book::create($data);
    }

    /**
     *
     * Query book list with filters and sorting based on 3th-part library
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return QueryBuilder::for(Book::class)
            ->join('authors','books.author_id', 'authors.id')
            ->join('categories','books.category_id', 'categories.id')
            ->allowedFilters(['title','author.first_name', 'author.last_name', 'category.name'])
            ->allowedSorts(['title','authors.first_name', 'authors.last_name', 'categories.name'])
            ->get();
    }
}
