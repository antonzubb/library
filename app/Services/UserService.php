<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    /**
     *
     * Create a new user
     *
     * @param array $credentials
     * @return User
     */
    public function create(array $credentials): User
    {
       return  User::create($this->prepareCredentials($credentials));
    }

    /**
     *
     * Prepare data before insert
     *
     * @param array $credentials
     * @return array
     */
    protected function prepareCredentials(array $credentials): array
    {
         $credentials['password'] = bcrypt($credentials['password']);
         return $credentials;
    }
}
