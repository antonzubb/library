<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryService
{
    /**
     * Creating new Category
     *
     * @param array $data
     * @return Category
     */
    public function create(array $data): Category
    {
        return Category::create($data);
    }

    /**
     * Getting all Categories
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Category::all();
    }

    /**
     * Update Category
     *
     * @param array $data
     * @param Category $category
     * @return bool
     */
    public function update(array $data, Category $category): bool
    {
        return $category->update($data);
    }

}
