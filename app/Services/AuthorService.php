<?php

namespace App\Services;

use App\Models\Author;
use Illuminate\Database\Eloquent\Collection;
use SebastianBergmann\CodeCoverage\TestFixture\C;

class AuthorService
{
    public function create(array $data): Author
    {
        return Author::create($data);
    }

    public function getAll(): Collection
    {
        return Author::all();
    }
}
