<?php

namespace App\Http\Requests;

class CreateAuthorRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:2|max:255',
            'last_name'  => 'required|string|min:2|max:255',
            'biography'  => 'required|string|min:15|max:1000',
        ];
    }
}
