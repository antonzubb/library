<?php

namespace App\Http\Requests;

class CreateBookRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author_id'   => 'required|exists:App\Models\Author,id',
            'category_id' => 'required|exists:App\Models\Category,id',
            'title'       => 'required|string|min:2|max:255',
            'description' => 'required|string|min:15|max:1000',
            'year'        => 'required|int'
        ];
    }
}
