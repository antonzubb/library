<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrUpdateCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $categories = $this->categoryService->getAll();

        return CategoryResource::collection($categories);
    }

    /**
     * Creating a new resource.
     *
     * @param CreateOrUpdateCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateOrUpdateCategoryRequest $request)
    {
        $this->categoryService->create($request->validated());

        return response()->json(['status' => true], 201);
    }

    /**
     *
     * Updating a new resource.
     *
     * @param CreateOrUpdateCategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateOrUpdateCategoryRequest $request, Category $category)
    {
        $isUpdated = $this->categoryService->update($request->validated(), $category);

        return response()->json(['status' => $isUpdated], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return CategoryResource
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

}
